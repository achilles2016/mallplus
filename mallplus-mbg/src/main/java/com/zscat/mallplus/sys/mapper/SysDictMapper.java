package com.zscat.mallplus.sys.mapper;

import com.zscat.mallplus.sys.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统配置信息表 Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2019-05-21
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
