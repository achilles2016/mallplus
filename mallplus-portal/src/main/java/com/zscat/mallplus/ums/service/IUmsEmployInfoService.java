package com.zscat.mallplus.ums.service;

import com.zscat.mallplus.ums.entity.UmsEmployInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zscat
 * @since 2019-07-02
 */
public interface IUmsEmployInfoService extends IService<UmsEmployInfo> {

}
